#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  7 12:17:31 2020

@author: mininet
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  7 12:11:54 2020

@author: mininet
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 17 09:07:51 2020

@author: mininet
"""

from mininet.net import Mininet

from mininet.node import Controller, RemoteController, OVSKernelSwitch, UserSwitch

from mininet.cli import CLI

from mininet.log import setLogLevel

from mininet.link import Link, TCLink

from functools import partial


 

def topology():
    
    
    
        sswitch=partial(OVSKernelSwitch,protocols='OpenFlow13')
        net = Mininet( controller=RemoteController, link=TCLink, switch=sswitch )

 

        # Add hosts and switches

        h1 = net.addHost( 'h1', ip="10.0.1.1/24", mac="00:00:00:00:00:01" )

        h2 = net.addHost( 'h2', ip="10.0.2.1/24", mac="00:00:00:00:00:02" )

        s3 = net.addSwitch( 's3')

        s1 = net.addSwitch( 's1')

        s2 = net.addSwitch( 's2')

        c0 = net.addController( 'c0', controller=RemoteController, ip='192.168.55.1', port=6653 )

 
        net.addLink( h1, s1 )
        net.addLink( h2, s2 )
        net.addLink( s1, s3 )
        net.addLink( s3, s2 )
        


        net.build()
        c0.start()

        s1.start( [c0] )
        s2.start( [c0] )
        s3.start( [c0] )

        h1.cmd("route add default gw 10.0.1.11 h1-eth0")
        h1.cmd("arp -i h1-eth0 -s 10.0.1.11 08:00:00:00:01:00")

        h2.cmd("route add default gw 10.0.2.11 h2-eth0")
        h2.cmd("arp -i h2-eth0 -s 10.0.2.11 08:00:00:00:02:00")

        

 

        print ("*** Running CLI")

        CLI( net )

 

        print ("*** Stopping network")

        net.stop()

      

if __name__ == '__main__':

    setLogLevel( 'info' )

    topology()  

