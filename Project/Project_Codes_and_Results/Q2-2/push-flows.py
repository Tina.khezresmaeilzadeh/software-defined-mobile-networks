from flowParser import flowParser
import requests
import os
import shutil
import argparse
from constants import (
		TIMEOUT,
		SLAVE_NODE_INVENTORY,
		SLAVE_FLOW_INVENTORY
	)

flows = {
	"openflow:1":[
		("table=0,eth_type=0x806,actions=normal", 0, 1),
		("table=0,eth_type=0x800,priority=100,nw_dst=10.0.1.0/24,actions=normal", 0, 2),
		("table=0,eth_type=0x800,priority=100,nw_dst=10.0.2.0/24,actions=output:2", 0, 3)
		
		
		
	],
	"openflow:2":[
		("table=0,eth_type=0x806,actions=normal", 0, 1),
		("table=0,eth_type=0x800,priority=100,nw_dst=10.0.2.0/24,actions=normal", 0, 2),
		("table=0,eth_type=0x800,priority=100,nw_dst=10.0.1.0/24,actions=output:2", 0, 3)
		
		
		
	],
	"openflow:3": [
		("table=0,priority=100,eth_type=0x806,arp_tpa=10.0.2.0/24,actions=output:2", 0, 1),
		("table=0,priority=100,eth_type=0x806,arp_tpa=10.0.1.0/24,actions=output:1", 0, 2),
		("table=0,priority=100,eth_type=0x0800,nw_dst=10.0.2.0/24,actions=mod_dl_dst:00:00:00:00:00:02,output:2", 0, 3),
		("table=0,priority=100,eth_type=0x0800,nw_dst=10.0.1.0/24,actions=mod_dl_dst:00:00:00:00:00:01,output:1", 0, 4)
	]
}

if os.path.isdir('./parsed_flows'):
	shutil.rmtree('parsed_flows')
os.mkdir('parsed_flows')

for sw_name in flows.keys():
	for tp in flows[sw_name]:
		flow = tp[0]
		table = tp[1]
		flow_id = tp[2]
		flow_file_name = sw_name + '_' + str(table) + '_' + str(flow_id)
		file = open('./parsed_flows/' + flow_file_name + '.xml', 'w')
		data = flowParser(flow, flow_id)
		file.write(data)
		file.close()


parser = argparse.ArgumentParser(description="Create and send flows to the controller")
parser.add_argument('--send', type=bool, 
	help='send flows to controller?')
parser.add_argument('--delete', type=bool, 
	help='delete existing flows?', default=True)

args = parser.parse_args()


if args.delete:
	for node in flows.keys():
		url = SLAVE_NODE_INVENTORY.format(
				"127.0.0.1",
				"8181",
				node
			)

		headers = {
			"Content-Type" : "application/xml"
		}

		auth = (
			"admin",
			"admin"
		)

		try:
			response = requests.delete(
					url=url,
					headers=headers,
					auth=auth,
					timeout=TIMEOUT
				)

			st = response.status_code

			if st != 200:
				print("Unable to delete for node {}, reason:".format(
						node
					))
				print(response.text)
		except Exception as e:
			raise(e)


if 1:
	for flow_file_name in os.listdir('./parsed_flows'):
		
		file = open('./parsed_flows/' + flow_file_name, "r")
		name = flow_file_name.replace('.xml', '')
		ls = name.split('_')

		node = ls[0]; table = ls[1]; flow_id = ls[2]

		url = SLAVE_FLOW_INVENTORY.format(
				"127.0.0.1",
				"8181",
				node,
				table,
				flow_id
			)

		print("\n\nURL: " + url)

		data = file.read()

		headers = {
			"Content-Type" : "application/xml"
		}

		auth = (
			"admin",
			"admin"
		)

		try:
			response = requests.put(
				url=url,
				data=data,
				headers=headers,
				timeout=TIMEOUT,
				auth=auth
			)

			st = response.status_code

			print("file " + flow_file_name + " was sent.")
			print("response : HTTP " + str(st))

			if st != 200 or st != 201:
				print(response.text)
		except:
			print("Something went wrong.")
			if response is not None:
				print(response)
