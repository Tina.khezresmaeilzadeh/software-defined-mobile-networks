#!/bin/bash


sudo ovs-ofctl -O OpenFlow13 del-flows s1
sudo ovs-ofctl -O OpenFlow13 del-flows s2
sudo ovs-ofctl -O OpenFlow13 del-flows r1

# add flows to s1
sudo ovs-ofctl -O OpenFlow13 add-flow s1 "table=0,in_port=1,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow s1 "table=0,in_port=2,actions=output=1"

# add flows to s2
sudo ovs-ofctl -O OpenFlow13 add-flow s2 "table=0,in_port=1,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow s2 "table=0,in_port=2,actions=output=1"


# add flows to r1
sudo ovs-ofctl -O OpenFlow13 add-flow r1 "table=0,arp,arp_tpa=10.0.2.0/24,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow r1 "table=0,arp,arp_tpa=10.0.1.0/24,actions=output=1"
sudo ovs-ofctl -O OpenFlow13 add-flow r1 "table=0,ip,nw_dst=10.0.2.1,actions=mod_dl_dst=00:00:00:00:00:02,output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow r1 "table=0,ip,nw_dst=10.0.1.1,actions=mod_dl_dst=00:00:00:00:00:01,output=1"



