#!/bin/bash


sudo ovs-ofctl -O OpenFlow13 del-flows s1
sudo ovs-ofctl -O OpenFlow13 del-flows s2

# add flows to s1
sudo ovs-ofctl -O OpenFlow13 add-flow s1 "table=0,arp,nw_dst=10.0.0.2,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow s1 "table=0,ip,nw_dst=10.0.0.2,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow s1 "table=0,ip,nw_dst=10.0.0.1,actions=output=1"
sudo ovs-ofctl -O OpenFlow13 add-flow s1 "table=0,arp,nw_dst=10.0.0.1,actions=output=1"


# add flows to s2
sudo ovs-ofctl -O OpenFlow13 add-flow s2 "table=0,arp,nw_dst=10.0.0.1,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow s2 "table=0,ip,nw_dst=10.0.0.1,actions=output=2"
sudo ovs-ofctl -O OpenFlow13 add-flow s2 "table=0,ip,nw_dst=10.0.0.2,actions=output=1"
sudo ovs-ofctl -O OpenFlow13 add-flow s2 "table=0,arp,nw_dst=10.0.0.2,actions=output=1"

