from mininet.net import Mininet
from mininet.node import RemoteController , OVSKernelSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.link import TCLink


def topology():
  net = Mininet(
  #controller=None,# None if we don't want any controller
  # RemoteController if you are using an
  # external SDN controller (like ODL)
  switch=OVSKernelSwitch ,
  link=TCLink
   )

  # Adding hosts

  h1 = net.addHost(
  name="h1",
  ip="10.0.0.1/24",
  mac="00:00:00:00:00:01"
  )
  
  h2 = net.addHost(
  name="h2",
  ip="10.0.0.2/24",
  mac="00:00:00:00:00:02"
  )

  # Adding switches

  s1 = net.addSwitch(
  name="s1"
  )
  
  s2 = net.addSwitch(
  name="s2"
  )

  # Adding controller (if any!)

  #c1 = net.addController(
  #name="your controller name"
  # Add an 'ip' and 'port' argument if you are
  # using a remote controller
  #)
   
  # Adding links

  net.addLink(h1, s1)
  net.addLink(h2, s2)
  net.addLink(s1, s2)

  # Start controller on switches (if any!)

  #s1.start([c1]) # More than one controller is possible!

  # Start CLI and build the network

  net.build()
  net.start()
  CLI(net)
  net.stop()

if __name__=='__main__':
  topology()
