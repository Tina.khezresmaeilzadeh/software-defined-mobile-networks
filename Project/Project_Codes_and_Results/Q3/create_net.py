from mininet.net import Mininet
from mininet.node import RemoteController , OVSKernelSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.link import TCLink



def topology(matrix):
  net = Mininet(
  controller=RemoteController,
  # if you are using an
  # external SDN controller (like ODL)
  switch=OVSKernelSwitch ,link=TCLink)
  
  
  for i in range(len(matrix)):
	  net.addSwitch(
	  name="s"+str(i+1)
	  )
		  
  	
  h1 = net.addHost(
  name="h1",
  ip="10.0.0.1/24",
  mac="00:00:00:00:00:01"
  )
  
  
  h2 = net.addHost(
  name="h2",
  ip="10.0.0.2/24",
  mac="00:00:00:00:00:02"
  )

  
  
  
  for i in range(len(matrix)):
  	  for j in range(i, len(matrix[i])): 
  	  	print(j)
  	  	if matrix[i][j] > 0:
  	  		net.addLink(net.getNodeByName("s"+str(i+1)), 
  	  			net.getNodeByName("s"+str(j+1)))
  	  			
  
  
   
   
  net.addLink(h1, net.getNodeByName("s1"))
  net.addLink(h2, net.getNodeByName("s" + str(len(matrix))))
  

  # Adding controller (if any!)

  c1 = net.addController(
  name="c1",
  ip="192.168.55.1",
  port=6633
  )
   
  for i in range(len(matrix)):
  	net.getNodeByName("s"+str(i+1)).start([c1])
	  



  # Start CLI and build the network

  net.build()
  net.start()
  CLI(net)
  net.stop()

if __name__=='__main__':
  topology([[0, 2, 3, 4], [2, 0, 0, 1], [3, 0, 0, 0], [1, 1, 0, 0]])

  
