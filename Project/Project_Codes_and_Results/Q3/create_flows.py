from spf import Graph
import numpy as np

def create_flow_from_path(path, destination_ip, flows, matrix):
	
	for i in range(len(path)-1):
		current_switch = path[i]
		next_switch = path[i+1]
		port_number = 0
		host_port_number = 1
		for index in range(len(matrix[i])):
			if matrix[i][index] != 0:
				host_port_number = host_port_number+1
			
		for index in range(int(next_switch[1:])):
			if matrix[int(current_switch[1:])-1][index] != 0:
				
				port_number = port_number + 1
				
		
		flow_index = len(flows["openflow:" + str(current_switch[1:])]) + 1
		
		nw_dst = destination_ip
		flow1 = ("table=0,eth_type=0x800,priority=100,nw_dst=" + nw_dst +  ",actions=output:"+str(port_number),0,flow_index)
		flow_index = flow_index + 1
		
		flow2 = ("table=0,eth_type=0x806,priority=100,arp_tpa=" + nw_dst +  ",actions=output:"+str(port_number),0,flow_index)
		flows["openflow:" + str(current_switch[1:])].append(flow1)
		flows["openflow:" + str(current_switch[1:])].append(flow2)
		
		
	destination_switch = path[-1]
	flow_index = len(flows["openflow:" + str(destination_switch[1:])]) + 1
	flow_destination1 = ("table=0,eth_type=0x806,priority=100,arp_tpa=" + nw_dst +  ",actions=output:"+str(host_port_number),0,flow_index)
	flow_index = flow_index + 1
	flow_destination2 = ("table=0,eth_type=0x800,priority=100,nw_dst=" + nw_dst +  ",actions=output:"+str(host_port_number),0,flow_index)
		
	
	
	flows["openflow:" + str(destination_switch[1:])].append(flow_destination1)
	flows["openflow:" + str(destination_switch[1:])].append(flow_destination2)
	return flows
	


def create_all_flows():
	matrix = np.array([[0, 2, 3, 4], [2, 0, 0, 1], [3, 0, 0, 0], [1, 1, 0, 0]])
	graph = Graph([
	    ("s1", "s2", 2),  ("s1", "s3", 3),  ("s1", "s4", 4), ("s2", "s1", 2),
	    ("s2", "s4", 1), ("s3", "s1", 3), ("s1", "s1", 3),  ("s4", "s1", 1),
	    ("s4", "s2", 1)])

	path1 = list(graph.dijkstra("s1", "s4"))
	path2 = list(graph.dijkstra("s4", "s1"))
	flows = {}
	for switch in path1:
		key = "openflow:" + str(switch[1:])
		if key not in flows.keys():
			flows[key] = []
		

	destination_ip1 = "10.0.0.2/24"
	destination_ip2 = "10.0.0.1/24"

	flows = create_flow_from_path(path1, destination_ip1, flows, matrix)
	flows = create_flow_from_path(path2, destination_ip2, flows, matrix)
	print(flows)	
	return flows


