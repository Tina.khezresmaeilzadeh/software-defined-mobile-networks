﻿## Project

This project includes virtual network development using mininet and OpenDayLight for adding flow on a designed network by converting flow formats to xml. The second part includes routing optimization using Dijkstra algorithm and then converting each path to a suitable flow entry.
