﻿## HW2

This homework includes link-level simulation of LTE downlink PHY (Physical Layer) using scrambler, turbo encoder and decoder, rate matcher, CRC generator and detector and also modulation and demodulation.
