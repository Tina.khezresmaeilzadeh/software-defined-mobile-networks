%% Loading .mat Files into Workspace

clear
clc
load('Q1_i_Rate1.mat');
load('Q1_i_Rate2.mat');
load('Q1_i_Rate3.mat');
load('Q1_i_Rate4.mat');
load('Q1_i_Rate5.mat');
load('Q1_i_Rate6.mat');
load('Early_Stopping.mat');

%% Plotting the Results

snr=-5:10;
semilogy(snr, BLR_Iter1_List)
hold on
semilogy(snr, BLR_Iter2_List)
hold on
semilogy(snr, BLR_Iter3_List)
hold on
semilogy(snr, BLR_Iter4_List)
hold on
semilogy(snr, BLR_Iter5_List)
hold on
semilogy(snr, BLR_Iter6_List)
hold on
semilogy(snr, BLR_Iter_Early_List)
grid on
title('QPSK BLER vs SNR')
xlabel('SNR');
ylabel('BLER');
legend('iter = 1', 'iter = 2', 'iter = 3', 'iter = 4', 'iter = 5', 'iter = 6',...
'early stopping') 
