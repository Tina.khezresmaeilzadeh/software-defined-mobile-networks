%% Part i
clear
clc
EbNo=1;
maxNumErrs=5e7;
maxNumBlocks=5e7;
FRM=2432-24; % Size of bit frame
maxIter = 1;
BLR_Iter1_List = [];
BLR_Iter2_List = [];
BLR_Iter3_List = [];
BLR_Iter4_List = [];
BLR_Iter5_List = [];
BLR_Iter6_List = [];
BLR_Iter_Early_List = [];
%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter1 = QPSK_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, 1, snr);
    disp(BLR_Iter1)
    BLR_Iter1_List = [BLR_Iter1_List BLR_Iter1];    
end
%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter2 = QPSK_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, 2, snr);
    disp(BLR_Iter2)
    BLR_Iter2_List = [BLR_Iter2_List BLR_Iter2];    
end
%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter3 = QPSK_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, 3, snr);
    disp(BLR_Iter3)
    BLR_Iter3_List = [BLR_Iter3_List BLR_Iter3];    
end
%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter4 = QPSK_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, 4, snr);
    disp(BLR_Iter4)
    BLR_Iter4_List = [BLR_Iter4_List BLR_Iter4];    
end
%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter5 = QPSK_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, 5, snr);
    disp(BLR_Iter5)
    BLR_Iter5_List = [BLR_Iter5_List BLR_Iter5];    
end

%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter6 = QPSK_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, 6, snr);
    disp(BLR_Iter6)
    BLR_Iter6_List = [BLR_Iter6_List BLR_Iter6];    
end

%%
parfor snr=-5:10 
    disp(snr);
    BLR_Iter_Early = QPSK_Early_Stopping_Simulation(EbNo, maxNumErrs, maxNumBlocks, FRM, snr);
    disp(BLR_Iter_Early)
    BLR_Iter_Early_List = [BLR_Iter_Early_List BLR_Iter_Early];    
end

%%
snr=-5:10;
semilogy(snr, BLR_Iter1_List)
hold on
semilogy(snr, BLR_Iter2_List)
hold on
semilogy(snr, BLR_Iter3_List)
hold on
semilogy(snr, BLR_Iter4_List)
hold on
semilogy(snr, BLR_Iter5_List)
hold on
semilogy(snr, BLR_Iter6_List)
hold on
semilogy(snr, BLR_Iter_Early_List)
grid on
title('QPSK BLER vs SND')
xlabel('SNR');
ylabel('BLER');
legend('iter = 1', 'iter = 2', 'iter = 3', 'iter = 4', 'iter = 5', 'iter = 6',...
'early stopping') 

    

%% Functions
function y=Modulator(u, Mode)

persistent QPSK QAM16 QAM64
if isempty(QPSK)
    QPSK = comm.PSKModulator(4, 'BitInput', true, ...
    'PhaseOffset', pi/4, 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [0 2 3 1]);
    QAM16 = comm.RectangularQAMModulator(16, 'BitInput',true,...
    'NormalizationMethod','Average power',...
    'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping',...
    [11 10 14 15 9 8 12 13 1 0 4 5 3 2 6 7]);
    QAM64 = comm.RectangularQAMModulator(64, 'BitInput',true,...
    'NormalizationMethod','Average power',...
    'SymbolMapping', 'Custom',...
    'CustomSymbolMapping',...
    [47 46 42 43 59 58 62 63 45 44 40 41 57 56 60 61 37 36 32 33 ...
    49 48 52 53 39 38 34 35 51 50 54 55 7 6 2 3 19 18 22 23 5 4 0 1 ...
    17 16 20 21 13 12 8 9 25 24 28 29 15 14 10 11 27 26 30 31]);
end

switch Mode
    case 1
        y=step(QPSK, u);
    case 2
        y=step(QAM16, u);
    case 3
        y=step(QAM64, u);
end

end

function y=DemodulatorHard(u, Mode)

persistent QPSK QAM16 QAM64
if isempty(QPSK)
    QPSK = comm.PSKDemodulator(...
    'ModulationOrder', 4, ...
    'BitOutput', true, ...
    'PhaseOffset', pi/4, 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [0 2 3 1]);
    QAM16 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 16, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [11 10 14 15 9 8 12 13 1 0 4 5 3 2 6 7]);
    QAM64 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 64, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', ...
    [47 46 42 43 59 58 62 63 45 44 40 41 57 56 60 61 37 ...
    36 32 33 49 48 52 53 39 38 34 35 51 50 54 55 7 6 2 3 ...
    19 18 22 23 5 4 0 1 17 16 20 21 13 12 8 9 25 24 28 29 ...
    15 14 10 11 27 26 30 31]);
end

switch Mode
    case 1
        y=QPSK.step(u);
    case 2
        y=QAM16.step(u);
    case 3
        y=QAM64.step(u);
    otherwise
        error('Invalid Modulation Mode. Use {1,2, or 3}');
end

end


function y=DemodulatorSoft(u, Mode, NoiseVar)

persistent QPSK QAM16 QAM64
if isempty(QPSK)
    QPSK = comm.PSKDemodulator(...
    'ModulationOrder', 4, ...
    'BitOutput', true, ...
    'PhaseOffset', pi/4, 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [0 2 3 1],...
    'DecisionMethod', 'Approximate log-likelihood ratio');
    QAM16 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 16, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [11 10 14 15 9 8 12 13 1 0 4 5 3 2 6 7],...
    'DecisionMethod', 'Approximate log-likelihood ratio', ...
    'VarianceSource', 'Input port');
    QAM64 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 64, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', ...
    [47 46 42 43 59 58 62 63 45 44 40 41 57 56 60 61 37 36 32 33 ...
    49 48 52 53 39 38 34 35 51 50 54 55 7 6 2 3 19 18 22 23 5 4 0 1 ...
    17 16 20 21 13 12 8 9 25 24 28 29 15 14 10 11 27 26 30 31],...
    'DecisionMethod', 'Approximate log-likelihood ratio', ...
    'VarianceSource', 'Input port');
end

switch Mode
    case 1
        y=step(QPSK, u);
    case 2
        y=step(QAM16,u);
    case 3
        y=step(QAM64, u);
    otherwise
        error('Invalid Modulation Mode. Use {1,2, or 3}');
end

end

function y = Scrambler(u, nS)
% Downlink scrambling
persistent hSeqGen hInt2Bit
if isempty(hSeqGen)
    maxG=43200;
    hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
    'FirstInitialConditions', [zeros(1, 30) 1], ...
    'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
    'SecondInitialConditionsSource', 'Input port',...
    'Shift', 1600,...
    'VariableSizeOutput', true,...
    'MaximumOutputSize', [maxG 1]);
    hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
end
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q =0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert initial condition to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate the scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
seq2=zeros(size(u));
seq2(:)=seq(1:numel(u),1);
% Scramble input with the scrambling sequence
y = xor(u, seq2);
end

function y = DescramblerSoft(u, nS)
% Downlink descrambling
persistent hSeqGen hInt2Bit;
if isempty(hSeqGen)
    maxG=43200;
    hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
    'FirstInitialConditions', [zeros(1, 30) 1], ...
    'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
    'SecondInitialConditionsSource', 'Input port',...
    'Shift', 1600,...
    'VariableSizeOutput', true,...
    'MaximumOutputSize', [maxG 1]);
    hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
end
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q=0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
seq2=zeros(size(u));
seq2(:)=seq(1:numel(u),1);
% If descrambler inputs are log-likelihood ratios (LLRs) then
% Convert sequence to a bipolar format
seq2 = 1-2.*seq2;
% Descramble
y = u.*seq2;

end

function y = DescramblerHard(u, nS)
% Downlink descrambling
persistent hSeqGen hInt2Bit;
if isempty(hSeqGen)
    maxG=43200;
    hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
    'FirstInitialConditions', [zeros(1, 30) 1], ...
    'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
    'SecondInitialConditionsSource', 'Input port',...
    'Shift', 1600,...
    'VariableSizeOutput', true,...
    'MaximumOutputSize', [maxG 1]);
    hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
end
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q=0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
% Descramble
y = xor(u(:,1), seq(:,1));
end

function y=TurboEncoder(u, lteIntrlvrIndices)
%#codegen

Turbo = comm.TurboEncoder('TrellisStructure', poly2trellis(4, [13 15], 13), ...
'InterleaverIndicesSource','Input port');

y=step(Turbo, u, lteIntrlvrIndices);
end

function y=TurboDecoder(u, lteIntrlvrIndices, maxIter)

Turbo = comm.TurboDecoder('TrellisStructure', poly2trellis(4, [13 15], 13),...
'InterleaverIndicesSource','Input port', ...
'NumIterations', maxIter);

y=step(Turbo, u, lteIntrlvrIndices);
end

function indices = lteIntrlvrIndices(blkLen)
%#codegen
[f1, f2] = getf1f2(blkLen);
Idx = (0:blkLen-1).';
indices = mod(f1*Idx + f2*Idx.^2, blkLen) + 1;
end

function y = CbCRCGenerator(u)
%#codegen
persistent hTBCRCGen
if isempty(hTBCRCGen)
    hTBCRCGen = comm.CRCGenerator('Polynomial',[1 1 zeros(1, 16) 1 1 0 0 0 1 1]);
end
% Transport block CRC generation
y = step(hTBCRCGen, u);
end


function y = CbCRCDetector(u)
%#codegen
persistent hTBCRC
if isempty(hTBCRC)
    hTBCRC = comm.CRCDetector('Polynomial', [1 1 zeros(1, 16) 1 1 0 0 0 1 1]);
end
% Transport block CRC generation
y = step(hTBCRC, u);
end

function [y, flag, iters]=TurboDecoder_crc(u, intrlvrIndices)
%#codegen
MAXITER=6;
persistent Turbo
if isempty(Turbo)
    Turbo = commLTETurboDecoder('InterleaverIndicesSource', 'Input port', ...
    'MaximumIterations',MAXITER);
end
[y, flag, iters] = step(Turbo, u, intrlvrIndices);
end


function BLR = QPSK_Simulation(EbNo, maxNumErrs, maxNumBits, FRM, maxIter, snr) 
Kplus=FRM+24;
Indices = lteIntrlvrIndices(Kplus);
ModulationMode=1;
k=2*ModulationMode;
CodingRate=Kplus/(3*Kplus+12);
noiseVar = 10.^(-snr/10);

numErrs = 0; numBits = 0; nS=0; numBlocks = 0; numErrBlocks=0;
while ((numErrs < maxNumErrs) && (numBits < maxNumBits))
    % Transmitter
    u = randi([0 1], FRM,1); % Randomly generated input bits
    data= CbCRCGenerator(u); % Code block CRC generator
    t0 = TurboEncoder(data, Indices); % Turbo Encoder
    t1 = Scrambler(t0, nS); % Scrambler
    t2 = Modulator(t1, ModulationMode); % Modulator
    % Channel
    c0 = awgn(t2, snr); % AWGN channel
    % Receiver
    r0 = DemodulatorSoft(c0, ModulationMode, noiseVar); % Demodulator
    r1 = DescramblerSoft(r0, nS); % Descrambler
    r2 = TurboDecoder(-r1, Indices, maxIter); % Turbo Decoder
    y = CbCRCDetector(r2); % Code block CRC detector
    % Measurements

    if y==u   
        numErrBlocks = numErrBlocks; % Update number of bit errors
    else
        numErrBlocks = numErrBlocks + 1;
        numErrs = numErrs + sum(abs(y-u));
        
    end
    
    numBits = numBits + FRM;
    numBlocks = numBlocks + 1;
    % Manage slot number with each subframe processed
    nS = nS + 2; nS = mod(nS, 20);
end

BLR = numErrBlocks/numBlocks;

end


function BLR = QPSK_Early_Stopping_Simulation(EbNo, maxNumErrs, maxNumBits, FRM,  snr) 
Kplus=FRM+24;
Indices = lteIntrlvrIndices(Kplus);
ModulationMode=1;
k=2*ModulationMode;
CodingRate=Kplus/(3*Kplus+12);
noiseVar = 10.^(-snr/10);

numErrs = 0; numBits = 0; nS=0; numBlocks = 0; numErrBlocks=0;
while ((numErrs < maxNumErrs) && (numBits < maxNumBits))
    % Transmitter
    u = randi([0 1], FRM,1); % Randomly generated input bits
    data= CbCRCGenerator(u); % Code block CRC generator
    t0 = TurboEncoder(data, Indices); % Turbo Encoder
    t1 = Scrambler(t0, nS); % Scrambler
    t2 = Modulator(t1, ModulationMode); % Modulator
    % Channel
    c0 = awgn(t2, snr); % AWGN channel
    % Receiver
    r0 = DemodulatorSoft(c0, ModulationMode, noiseVar); % Demodulator
    r1 = DescramblerSoft(r0, nS); % Descrambler
    [y,~,~] = TurboDecoder_crc(-r1, Indices);
    % Measurements

    if y==u   
        numErrBlocks = numErrBlocks; % Update number of bit errors
    else
        numErrBlocks = numErrBlocks + 1;
        numErrs = numErrs + sum(abs(y-u));
        
    end
    
    numBits = numBits + FRM;
    numBlocks = numBlocks + 1;
    % Manage slot number with each subframe processed
    nS = nS + 2; nS = mod(nS, 20);
end

BLR = numErrBlocks/numBlocks;

end