
clear
clc
% Channel Parameters
numTx = 1;
numRx = 1;
chanMdl ='flat-low-mobility';
chanSRate = 30.72e6;
corrLvl = 'Low';
maxNumErrs=5e7;
maxNumBits=5e7;
CodingRate = 1/3;
snr = 30;
chanSBW = 20e6;
[ber, Transmitted_Flat, Received_Flat] =chap6_ex05_crc(maxNumErrs, maxNumBits,...
CodingRate, snr,numTx,...
numRx, chanMdl, chanSRate, corrLvl, chanSBW);
Transmitted_Flat = reshape(Transmitted_Flat, 1, size(Transmitted_Flat,1)*size(Transmitted_Flat,2));
Received_Flat = reshape(Received_Flat, 1, size(Received_Flat,1)*size(Received_Flat,2));

%%
clear
clc
numTx = 1;
numRx = 1;
chanMdl ='frequency-selective-low-mobility';
chanSRate = 30.72e6;
corrLvl = 'Low';
maxNumErrs=5e7;
maxNumBits=5e7;
CodingRate = 1/3;
snr = 30;
chanSBW = 20e6;
[ber, Transmitted_Frequency_Selective, Received_Frequency_Selective] =chap6_ex05_crc(maxNumErrs,...
maxNumBits, CodingRate, snr,numTx,...
numRx, chanMdl, chanSRate, corrLvl, chanSBW);
Transmitted_Frequency_Selective = reshape(Transmitted_Frequency_Selective, 1,...
    size(Transmitted_Frequency_Selective,1)*size(Transmitted_Frequency_Selective,2));
Received_Frequency_Selective = reshape(Received_Frequency_Selective, 1,...
    size(Received_Frequency_Selective,1)*size(Received_Frequency_Selective,2));

%% Functions
function y=Modulator(u, Mode)

persistent QPSK QAM16 QAM64
if isempty(QPSK)
    QPSK = comm.PSKModulator(4, 'BitInput', true, ...
    'PhaseOffset', pi/4, 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [0 2 3 1]);
    QAM16 = comm.RectangularQAMModulator(16, 'BitInput',true,...
    'NormalizationMethod','Average power',...
    'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping',...
    [11 10 14 15 9 8 12 13 1 0 4 5 3 2 6 7]);
    QAM64 = comm.RectangularQAMModulator(64, 'BitInput',true,...
    'NormalizationMethod','Average power',...
    'SymbolMapping', 'Custom',...
    'CustomSymbolMapping',...
    [47 46 42 43 59 58 62 63 45 44 40 41 57 56 60 61 37 36 32 33 ...
    49 48 52 53 39 38 34 35 51 50 54 55 7 6 2 3 19 18 22 23 5 4 0 1 ...
    17 16 20 21 13 12 8 9 25 24 28 29 15 14 10 11 27 26 30 31]);
end

switch Mode
    case 1
        y=step(QPSK, u);
    case 2
        y=step(QAM16, u);
    case 3
        y=step(QAM64, u);
end

end

function y=DemodulatorHard(u, Mode)

persistent QPSK QAM16 QAM64
if isempty(QPSK)
    QPSK = comm.PSKDemodulator(...
    'ModulationOrder', 4, ...
    'BitOutput', true, ...
    'PhaseOffset', pi/4, 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [0 2 3 1]);
    QAM16 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 16, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [11 10 14 15 9 8 12 13 1 0 4 5 3 2 6 7]);
    QAM64 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 64, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', ...
    [47 46 42 43 59 58 62 63 45 44 40 41 57 56 60 61 37 ...
    36 32 33 49 48 52 53 39 38 34 35 51 50 54 55 7 6 2 3 ...
    19 18 22 23 5 4 0 1 17 16 20 21 13 12 8 9 25 24 28 29 ...
    15 14 10 11 27 26 30 31]);
end

switch Mode
    case 1
        y=QPSK.step(u);
    case 2
        y=QAM16.step(u);
    case 3
        y=QAM64.step(u);
    otherwise
        error('Invalid Modulation Mode. Use {1,2, or 3}');
end

end


function y=DemodulatorSoft(u, Mode, NoiseVar)

persistent QPSK QAM16 QAM64
if isempty(QPSK)
    QPSK = comm.PSKDemodulator(...
    'ModulationOrder', 4, ...
    'BitOutput', true, ...
    'PhaseOffset', pi/4, 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [0 2 3 1],...
    'DecisionMethod', 'Approximate log-likelihood ratio');
    QAM16 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 16, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', [11 10 14 15 9 8 12 13 1 0 4 5 3 2 6 7],...
    'DecisionMethod', 'Approximate log-likelihood ratio', ...
    'VarianceSource', 'Input port');
    QAM64 = comm.RectangularQAMDemodulator(...
    'ModulationOrder', 64, ...
    'BitOutput', true, ...
    'NormalizationMethod', 'Average power', 'SymbolMapping', 'Custom', ...
    'CustomSymbolMapping', ...
    [47 46 42 43 59 58 62 63 45 44 40 41 57 56 60 61 37 36 32 33 ...
    49 48 52 53 39 38 34 35 51 50 54 55 7 6 2 3 19 18 22 23 5 4 0 1 ...
    17 16 20 21 13 12 8 9 25 24 28 29 15 14 10 11 27 26 30 31],...
    'DecisionMethod', 'Approximate log-likelihood ratio', ...
    'VarianceSource', 'Input port');
end

switch Mode
    case 1
        y=step(QPSK, u);
    case 2
        y=step(QAM16,u, NoiseVar);
    case 3
        y=step(QAM64, u, NoiseVar);
    otherwise
        error('Invalid Modulation Mode. Use {1,2, or 3}');
end

end

function y = Scrambler(u, nS)
% Downlink scrambling
persistent hSeqGen hInt2Bit
if isempty(hSeqGen)
    maxG=43200;
    hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
    'FirstInitialConditions', [zeros(1, 30) 1], ...
    'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
    'SecondInitialConditionsSource', 'Input port',...
    'Shift', 1600,...
    'VariableSizeOutput', true,...
    'MaximumOutputSize', [maxG 1]);
    hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
end
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q =0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert initial condition to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate the scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
seq2=zeros(size(u));
seq2(:)=seq(1:numel(u),1);
% Scramble input with the scrambling sequence
y = xor(u, seq2);
end

function y = DescramblerSoft(u, nS)
% Downlink descrambling
persistent hSeqGen hInt2Bit;
if isempty(hSeqGen)
    maxG=43200;
    hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
    'FirstInitialConditions', [zeros(1, 30) 1], ...
    'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
    'SecondInitialConditionsSource', 'Input port',...
    'Shift', 1600,...
    'VariableSizeOutput', true,...
    'MaximumOutputSize', [maxG 1]);
    hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
end
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q=0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
seq2=zeros(size(u));
seq2(:)=seq(1:numel(u),1);
% If descrambler inputs are log-likelihood ratios (LLRs) then
% Convert sequence to a bipolar format
seq2 = 1-2.*seq2;
% Descramble
y = u.*seq2;

end

function y = DescramblerHard(u, nS)
% Downlink descrambling
persistent hSeqGen hInt2Bit;
if isempty(hSeqGen)
    maxG=43200;
    hSeqGen = comm.GoldSequence('FirstPolynomial',[1 zeros(1, 27) 1 0 0 1],...
    'FirstInitialConditions', [zeros(1, 30) 1], ...
    'SecondPolynomial', [1 zeros(1, 27) 1 1 1 1],...
    'SecondInitialConditionsSource', 'Input port',...
    'Shift', 1600,...
    'VariableSizeOutput', true,...
    'MaximumOutputSize', [maxG 1]);
    hInt2Bit = comm.IntegerToBit('BitsPerInteger', 31);
end
% Parameters to compute initial condition
RNTI = 1;
NcellID = 0;
q=0;
% Initial conditions
c_init = RNTI*(2^14) + q*(2^13) + floor(nS/2)*(2^9) + NcellID;
% Convert to binary vector
iniStates = step(hInt2Bit, c_init);
% Generate scrambling sequence
nSamp = size(u, 1);
seq = step(hSeqGen, iniStates, nSamp);
% Descramble
y = xor(u(:,1), seq(:,1));
end

function y=TurboEncoder(u, lteIntrlvrIndices)
%#codegen

Turbo = comm.TurboEncoder('TrellisStructure', poly2trellis(4, [13 15], 13), ...
'InterleaverIndicesSource','Input port');

y=step(Turbo, u, lteIntrlvrIndices);
end

function y=TurboDecoder(u, lteIntrlvrIndices, maxIter)

Turbo = comm.TurboDecoder('TrellisStructure', poly2trellis(4, [13 15], 13),...
'InterleaverIndicesSource','Input port', ...
'NumIterations', maxIter);

y=step(Turbo, u, lteIntrlvrIndices);
end

function indices = lteIntrlvrIndices(blkLen)
%#codegen
[f1, f2] = getf1f2(blkLen);
Idx = (0:blkLen-1).';
indices = mod(f1*Idx + f2*Idx.^2, blkLen) + 1;
end

function y = CbCRCGenerator(u)
%#codegen
persistent hTBCRCGen
if isempty(hTBCRCGen)
    hTBCRCGen = comm.CRCGenerator('Polynomial',[1 1 zeros(1, 16) 1 1 0 0 0 1 1]);
end
% Transport block CRC generation
y = step(hTBCRCGen, u);
end


function y = CbCRCDetector(u)
%#codegen
persistent hTBCRC
if isempty(hTBCRC)
    hTBCRC = comm.CRCDetector('Polynomial', [1 1 zeros(1, 16) 1 1 0 0 0 1 1]);
end
% Transport block CRC generation
y = step(hTBCRC, u);
end

function [y, flag, iters]=TurboDecoder_crc(u, intrlvrIndices)
%#codegen
MAXITER=6;
persistent Turbo
if isempty(Turbo)
    Turbo = commLTETurboDecoder('InterleaverIndicesSource', 'Input port', ...
    'MaximumIterations',MAXITER);
end
[y, flag, iters] = step(Turbo, u, intrlvrIndices);
end


function y= RateMatcher(in, Kplus, Rate)
% Rate matching per coded block, with and without the bit selection.
D = Kplus+4;
if numel(in)~=3*D, error('Kplus (2nd argument) times 3 must be size of input 1.');end
% Parameters
colTcSb = 32;
rowTcSb = ceil(D/colTcSb);
Kpi = colTcSb * rowTcSb;
Nd = Kpi - D;
% Bit streams
d0 = in(1:3:end); % systematic
d1 = in(2:3:end); % parity 1st
d2 = in(3:3:end); % parity 2nd
i0=(1:D)';
Index=indexGenerator(i0,colTcSb, rowTcSb, Nd);
Index2=indexGenerator2(i0,colTcSb, rowTcSb, Nd);
% Sub-block interleaving - per stream
v0 = subBlkInterl(d0,Index);
v1 = subBlkInterl(d1,Index);
v2 = subBlkInterl(d2,Index2);
vpre=[v1,v2].';
v12=vpre(:);
% Concat 0, interleave 1, 2 sub-blk streams
% Bit collection
wk = zeros(numel(in), 1);
wk(1:D) = remove_dummy_bits( v0 );
wk(D+1:end) = remove_dummy_bits( v12);
% Apply rate matching
N=ceil(D/Rate);
%%%%%%%%%%%%%%
if mod(N,6)~=0
    N=N-mod(N,6);
end
%%%%%%%%%%%%%%%
y=wk(1:N);
end


function v = indexGenerator(d, colTcSb, rowTcSb, Nd)
% Sub-block interleaving - for d0 and d1 streams only
colPermPat = [0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30,...
1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31];
% For 1 and 2nd streams only
y = [NaN*ones(Nd, 1); d]; % null (NaN) filling
inpMat = reshape(y, colTcSb, rowTcSb).';
permInpMat = inpMat(:, colPermPat+1);
v = permInpMat(:);
end


function v = indexGenerator2(d, colTcSb, rowTcSb, Nd)
% Sub-block interleaving - for d2 stream only
colPermPat = [0, 16, 8, 24, 4, 20, 12, 28, 2, 18, 10, 26, 6, 22, 14, 30,...
1, 17, 9, 25, 5, 21, 13, 29, 3, 19, 11, 27, 7, 23, 15, 31];
pi = zeros(colTcSb*rowTcSb, 1);
for i = 1 : length(pi)
pi(i) = colPermPat(floor((i-1)/rowTcSb)+1) + colTcSb*(mod(i-1, rowTcSb)) + 1;
end
% For 3rd stream only
y = [NaN*ones(Nd, 1); d]; % null (NaN) filling
inpMat = reshape(y, colTcSb, rowTcSb).';
ytemp = inpMat.';
y = ytemp(:);
v = y(pi);
end


function out = remove_dummy_bits( wk )
%UNTITLED5 Summary of this function goes here
%out = wk(find(̃isnan(wk)));
out=wk(isfinite(wk));
end



function out=subBlkInterl(d0,Index)
out=zeros(size(Index));
IndexG=find(~isnan(Index)==1);
IndexB=find(isnan(Index)==1);
out(IndexG)=d0(Index(IndexG));
Nd=numel(IndexB);
out(IndexB)=nan*ones(Nd,1);
end


function out = RateDematcher(in, Kplus)
% Undoes the Rate matching per coded block.
%#codegen
% Parameters
colTcSb = 32;
D = Kplus+4;
rowTcSb = ceil(D/colTcSb);
Kpi = colTcSb * rowTcSb;
Nd = Kpi - D;
tmp=zeros(3*D,1);
tmp(1:numel(in))=in;
% no bit selection - assume full buffer passed in
i0=(1:D)';
Index= indexGenerator(i0,colTcSb, rowTcSb, Nd);
Index2= indexGenerator2(i0,colTcSb, rowTcSb, Nd);
Indexpre=[Index,Index2+D].';
Index12=Indexpre(:);
% Bit streams
tmp0=tmp(1:D);
tmp12=tmp(D+1:end);
v0 = subBlkDeInterl(tmp0, Index);
d12=subBlkDeInterl(tmp12, Index12);
v1=d12(1:D);
v2=d12(D+(1:D));
% Interleave 1, 2, 3 sub-blk streams - for turbo decoding
temp = [v0 v1 v2].';
out = temp(:);
end



function out=subBlkDeInterl(in,Index)
out=zeros(size(in));
IndexG=find(~isnan(Index)==1);
IndexOut=Index(IndexG);
out(IndexOut)=in;
end


function [y, yPg] = MIMOFadingChan(in, numTx, numRx, chanMdl, chanSRate, corrLvl)
% MIMOFadingChan
%#codegen

switch chanMdl
    case 'flat-low-mobility'
        PathDelays = 100*(1/chanSRate);
        PathGains = -17.2;
        Doppler=0;
        ChannelType =1;
    case 'flat-high-mobility'
        PathDelays = 0*(1/chanSRate);
        PathGains = 0;
        Doppler=70;
        ChannelType =1;
    case 'frequency-selective-low-mobility'
        PathDelays = [0 10 20 30 100]*(1/chanSRate);
        PathGains = [0 -3 -6 -8 -17.2];
        Doppler=0;
        ChannelType =1;
    case 'frequency-selective-high-mobility'
        PathDelays = [0 10 20 30 100]*(1/chanSRate);
        PathGains = [0 -3 -6 -8 -17.2];
        Doppler=70;
        ChannelType =1;
    case 'EPA 0Hz'
        PathDelays = [0 30 70 90 110 190 410]*1e-9;
        PathGains = [0 -1 -2 -3 -8 -17.2 -20.8];
        Doppler=0;
        ChannelType =1;
    otherwise
        ChannelType =2;
        AntConfig=char([48+numTx,'x',48+numRx]);
end
% Initialize objects
persistent chanObj;
if isempty(chanObj)
    if ChannelType ==1
        chanObj = comm.MIMOChannel('SampleRate', chanSRate, ...
        'MaximumDopplerShift', Doppler, ...
        'PathDelays', PathDelays,...
        'AveragePathGains', PathGains,...
        'RandomStream', 'mt19937ar with seed',...
        'Seed', 100,...
        'NumTransmitAntennas', numTx,...
        'TransmitCorrelationMatrix', eye(numTx),...
        'NumReceiveAntennas', numRx,...
        'ReceiveCorrelationMatrix', eye(numRx),...
        'PathGainsOutputPort', true,...
        'NormalizePathGains', true,...
        'NormalizeChannelOutputs', true);
    else
        chanObj = comm.LTEMIMOChannel('SampleRate', chanSRate, ...
        'Profile', chanMdl, ...
        'AntennaConfiguration', AntConfig, ...
        'CorrelationLevel', corrLvl,...
        'RandomStream', 'mt19937ar with seed',...
        'Seed', 100,...
        'PathGainsOutputPort', true);
    end
end
[y, yPg] = step(chanObj, in);
end


function k = Plot_PSD(Fs, x, BW)
N = length(x);
xdft = fft(x);
xdft = xdft(1:N/2+1);
psdx = (1/(Fs*N)) * abs(xdft).^2;
psdx(2:end-1) = 2*psdx(2:end-1);
freq = 0:Fs/length(x):Fs/2;
plot(freq/(10^6),10*log10(psdx))
grid on
title('Periodogram Using FFT')
xlabel('Frequency (MHz)')
ylabel('Power/Frequency (dB/Hz)')
xlim([0 BW/(10^6)]);
end

function [ber, Transmitted, Received] =chap6_ex05_crc(maxNumErrs, maxNumBits, CodingRate, snr,numTx, numRx, chanMdl, chanSRate, corrLvl, chanSBW)

FRM=2432-24; % Size of bit frame
Kplus=FRM+24;
Indices = lteIntrlvrIndices(Kplus);
ModulationMode=3;
k=2*ModulationMode;
noiseVar = 10.^(-snr/10);
maxIter = 6;
Hist=dsp.Histogram('LowerLimit', 1, 'UpperLimit', maxIter, 'NumBins', maxIter,...
'RunningHistogram', true);
Transmitted = zeros(40, 1218);
Received = zeros(40, 1218);
numErrs = 0; numBits = 0; nS=0; counter = 0;
while ((numErrs < maxNumErrs) && (numBits < maxNumBits))
    counter = counter + 1;
    % Transmitter
    u = randi([0 1], FRM,1); % Randomly generated input bits
    data= CbCRCGenerator(u); % Transport block CRC code
    t0 = TurboEncoder(data, Indices); % Turbo Encoder
    t1= RateMatcher(t0, Kplus, CodingRate); % Rate Matcher
    t2 = Scrambler(t1, nS); % Scrambler
    t3 = Modulator(t2, ModulationMode); % Modulator
    Transmitted(counter,:) = t3;
    
    % Channel
    [rxFade,~] = MIMOFadingChan(t3, numTx, numRx, chanMdl, chanSRate, corrLvl);
    c0 = awgn(rxFade, snr); % AWGN channel
    % Receiver
    Received(counter,:) = c0;
    r0 = DemodulatorSoft(c0, ModulationMode, noiseVar); % Demodulator
    r1 = DescramblerSoft(r0, nS); % Descrambler
    r2 = RateDematcher(r1, Kplus); % Rate Matcher
    [y,~, iters] = TurboDecoder_crc(-r2, Indices); % Turbo Decoder
    % Measurements
    numErrs = numErrs + sum(abs(y-u));
    numBits = numBits + FRM;
    itersHist = step(Hist, iters); % Update histogram
    % Manage slot number with each subframe processed
    nS = nS + 2; nS = mod(nS, 20);
    if counter >= 10
        break
    end
end


ber = numErrs/numBits; % Compute Bit Error Rate (BER)
end