%% Loading .mat Files into Workspace

clear
clc
%% Loading data into BLER_Matrix
BLER_Martix = ones(39,26);
% Loading QPSK BLRs
counter = 0;
load('Q1_ii_Rate35_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate4_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate45_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter2_List;
load('Q1_ii_Rate5_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate55_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate6_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate65_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate7_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate75_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate8_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate85_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate9_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate95_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
% Loading QAM16 BLRs


load('Q1_ii_Rate35_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate4_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate45_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate5_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate55_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate6_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate65_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate7_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate75_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate8_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate85_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate9_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate95_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;

% Loading QAM64 BLRs


load('Q1_ii_Rate35_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate4_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate45_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate5_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate55_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate6_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate65_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate7_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate75_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate8_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate85_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate9_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate95_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;

%% Plotting BLRs vs SNR

snr=-5:20;
for i=1:size(BLER_Martix,1)
    semilogy(snr, BLER_Martix(i,:));
    hold on
end
grid on
title('BLER vs SNR')
xlabel('SNR');
ylabel('BLER');