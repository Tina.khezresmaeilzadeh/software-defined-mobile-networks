%% Loading .mat Files into Workspace

clear
clc
%% Loading data into BLER_Matrix
BLER_Martix = ones(39,26);
% Loading QPSK BLRs
counter = 0;
load('Q1_ii_Rate35_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate4_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate45_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter2_List;
load('Q1_ii_Rate5_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate55_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate6_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate65_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate7_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate75_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate8_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate85_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate9_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate95_QPSK');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
% Loading QAM16 BLRs


load('Q1_ii_Rate35_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate4_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate45_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate5_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate55_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate6_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate65_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate7_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate75_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate8_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate85_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate9_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate95_QAM16');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;

% Loading QAM64 BLRs


load('Q1_ii_Rate35_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate4_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate45_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate5_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate55_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate6_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate65_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate7_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate75_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate8_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate85_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate9_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;
load('Q1_ii_Rate95_QAM64');
counter = counter + 1;
BLER_Martix(counter,:) = BLR_Iter1_List;

%% Plotting BLRs vs SNR

snr=-5:20;
for i=1:size(BLER_Martix,1)
    semilogy(snr, BLER_Martix(i,:));
    hold on
end
semilogy(snr, 0.1*ones(size(snr)));
grid on
title('BLER vs SNR')
xlabel('SNR');
ylabel('BLER');

%% Gathering the index of the intersection of each graph with BLR = 0.1 into intersection_snr
intersection_snr = ones(size(BLER_Martix,1),1);

for j=1:size(BLER_Martix,1)
    for k=1:size(BLER_Martix,2)
        if BLER_Martix(j,k)<=0.1
            intersection_snr(j)=k;
            break
        end
    end
end
intersection_snr(size(BLER_Martix,1)) = size(BLER_Martix,2);
%% Finding the proper modulation mode and coding rate for each snr
snr = -5:20;
CQI = ones(length(snr),1);
Num_Code_Rates = 13;
Coding_Rate = 0.35:0.05:0.95;
for i=1:length(snr)
    for j=length(intersection_snr):-1:1
        if snr(intersection_snr(j))<=snr(i)
            CQI(i) = j;
            break
        end
    end
    
end
    
modulation_mode = zeros(length(CQI),1);
coding_rate = zeros(length(CQI),1);


for i=1:length(CQI)
    modulation_mode(i) = fix(CQI(i)/13) + 1;
    if CQI(i)<= Num_Code_Rates
        coding_rate(i) = Coding_Rate(CQI(i));
    elseif CQI(i)<= Num_Code_Rates*2
        coding_rate(i) = Coding_Rate(CQI(i)-Num_Code_Rates);
    else
        coding_rate(i) = Coding_Rate(CQI(i)-Num_Code_Rates*2);
    end
         
end
figure
plot(snr, coding_rate);
title('the mapping function between rate and snr');
xlabel('SNR');
ylabel('Coding Rate');
grid on
    
%% Finding maximum throughput for each snr
Throughput = zeros(Num_Code_Rates*3,26);



for i=1:size(Throughput,1)
    if i<= 13
        Bit_per_Modulated_Symbol = 2;
        index = i;
    elseif i<=26
        Bit_per_Modulated_Symbol = 4;
        index = i-13;
    else
        Bit_per_Modulated_Symbol = 6;
        index=i-26;
    end
    
    Throughput(i,:) = (1 - BLER_Martix(i,:))*Bit_per_Modulated_Symbol*Coding_Rate(index);
end

Maximum_Throughput = zeros(length(snr),1);

for i=1:length(snr)
    Maximum_Throughput(i) = Throughput(CQI(i),i);    
end

%% Plotting Maximum Throughput vs. SNR
plot(snr, Maximum_Throughput);
title('Maximum Throughput vs. snr');
xlabel('SNR');
ylabel('Maximum Throughput');
grid on