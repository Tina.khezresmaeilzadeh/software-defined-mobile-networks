clear
clc
% PDSCH
numTx = 1; % Number of transmit antennas
numRx = 1; % Number of receive antennas
chanBW = 4; % Index to chanel bandwidth used [1,....6]
contReg = 1; % No. of OFDM symbols dedictaed to control information [1,...,3]
modType = 2; % Modulation type [1, 2, 3] for ['QPSK,'16QAM','64QAM']
% DLSCH
cRate = 1/3; % Rate matching target coding rate
maxIter = 6; % Maximum number of turbo decoding terations
fullDecode = 0; % Whether "full" or "early stopping" turbo decoding is performed
% Channel model
chanMdl = 'frequency-selective-high-mobility';
corrLvl = 'Low';
% Simulation parametrs
Eqmode = 2; % Type of equalizer used [1,2] for ['ZF', 'MMSE']
chEstOn = 1; % Whether channel estimation is done or ideal channel model used
maxNumErrs = 5e7; % Maximum number of errors found before simulation stops
maxNumBits = 5e7; % Maximum number of bits processed before simulation stops
visualsOn = 1; % Whether to visualize channel response and constellations
FRM = 2432-24;


disp('Simulating the LTE Mode 1: Single Tx and Rx antrenna');
%% Set simulation parametrs & initialize parameter structures

[prmLTEPDSCH, prmLTEDLSCH, prmMdl] = commlteSISO_initialize( chanBW,...
contReg, modType, Eqmode,...
cRate,maxIter, fullDecode, chanMdl, corrLvl, chEstOn, maxNumErrs, maxNumBits, FRM);
clear chanBW contReg numTx numRx modType Eqmode cRate maxIter fullDecode...
chanMdl corrLvl chEstOn maxNumErrs maxNumBits;
%
hPBer = comm.ErrorRate;
iter=numel(prmMdl.snrdBs);
snrdB=prmMdl.snrdBs(iter);
maxNumErrs=prmMdl.maxNumErrs(iter);
maxNumBits=prmMdl.maxNumBits(iter);
% Simulation loop

nS = 0; % Slot number, one of [0:2:18]
Measures = zeros(3,1); %initialize BER output
while (( Measures(2)< maxNumErrs) && (Measures(3) < maxNumBits))
    [dataIn, dataOut, txSig, rxSig, dataRx, yRec, csr] = ...
    commlteSISO_step(nS, snrdB, prmLTEDLSCH, prmLTEPDSCH, prmMdl);
    % Calculate bit errors
    Measures = step(hPBer, dataIn, dataOut);
    % Visualize constellations and spectrum
    if visualsOn, zVisualize( prmLTEPDSCH, txSig, rxSig, yRec, dataRx, csr, nS);end
    % Update subframe number
    nS = nS + 2; if nS > 19, nS = mod(nS, 20); end
end
disp(Measures);


%% Functions


function [dataIn, dataOut, txSig, rxSig, dataRx, yRec, csr]...
= commlteSISO_step(nS, snrdB, prmLTEDLSCH, prmLTEPDSCH, prmMdl)
% TX
% Generate payload
dataIn = genPayload(nS, prmLTEDLSCH.TBLenVec);
% Transport block CRC generation
tbCrcOut1 =CRCgenerator(dataIn);
% Channel coding includes - CB segmentation, turbo coding, rate matching,
% bit selection, CB concatenation - per codeword
[data, Kplus1, C1] = lteTbChannelCoding(tbCrcOut1, nS, prmLTEDLSCH, prmLTEPDSCH);
%Scramble codeword
scramOut = lteScramble(data, nS, 0, prmLTEPDSCH.maxG);
% Modulate
modOut = Modulator(scramOut, prmLTEPDSCH.modType);
% Generate Cell-Specific Reference (CSR) signals
csr = CSRgenerator(nS, prmLTEPDSCH.numTx);
% Resource grid filling
E=8*prmLTEPDSCH.Nrb;
csr_ref=reshape(csr(1:E),2*prmLTEPDSCH.Nrb,4);
txGrid = REmapper_1Tx(modOut, csr_ref, nS, prmLTEPDSCH);
% OFDM transmitter
txSig = OFDMTx(txGrid, prmLTEPDSCH);
% Channel
% SISO Fading channel
[rxFade, chPathG] = MIMOFadingChan(txSig, prmLTEPDSCH, prmMdl);
idealhD = lteIdChEst(prmLTEPDSCH, prmMdl, chPathG, nS);
% Add AWG noise
nVar = 10.^(0.1.*(-snrdB));
rxSig = AWGNChannel(rxFade, nVar);
% RX
% OFDM Rx
rxGrid = OFDMRx(rxSig, prmLTEPDSCH);
% updated for numLayers -> numTx
[dataRx, csrRx, idx_data] = REdemapper_1Tx(rxGrid, nS, prmLTEPDSCH);
% MIMO channel estimation
if prmMdl.chEstOn
chEst = ChanEstimate_1Tx(prmLTEPDSCH, csrRx, csr_ref, 'interpolate');
hD=chEst(idx_data).';
else
hD = idealhD;
end
% Frequency-domain equalizer
yRec = Equalizer(dataRx, hD, nVar, prmLTEPDSCH.Eqmode);
% Demodulate
demodOut = DemodulatorSoft(yRec, prmLTEPDSCH.modType, nVar);
% Descramble both received codewords
rxCW = lteDescramble(demodOut, nS, 0, prmLTEPDSCH.maxG);
% Channel decoding includes - CB segmentation, turbo decoding, rate dematching
[decTbData1,~,~] = lteTbChannelDecoding(nS, rxCW, Kplus1, C1, prmLTEDLSCH,...
prmLTEPDSCH);

% Transport block CRC detection
[dataOut, ] = CRCdetector(decTbData1);
end

