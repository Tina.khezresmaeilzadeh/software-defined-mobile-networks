
clear all
clear functions
disp('Simulating the LTE Mode 1: Single Tx and Rx antrenna');
%% Set simulation parametrs & initialize parameter structures
commlteSISO_params;
[prmLTEPDSCH, prmLTEDLSCH, prmMdl] = commlteSISO_initialize(chanBW,...
contReg, modType, Eqmode,...
cRate,maxIter, fullDecode, chanMdl, corrLvl, chEstOn, maxNumErrs, maxNumBits);
clear chanBW contReg numTx numRx modType Eqmode cRate maxIter fullDecode ...
chanMdl corrLvl chEstOn maxNumErrs maxNumBits;

hPBer = comm.ErrorRate;
iter=numel(prmMdl.snrdBs);
snrdB=prmMdl.snrdBs(iter);
maxNumErrs=prmMdl.maxNumErrs(iter);
maxNumBits=prmMdl.maxNumBits(iter);
Transmitted_Length = 30720;
nS = 0; 
Measures = zeros(3,1); 
transmitted = zeros(5,Transmitted_Length);
received = zeros(5,Transmitted_Length);
counter = 0;
while (( Measures(2)< maxNumErrs) && (Measures(3) < maxNumBits))
[dataIn, dataOut, txSig, rxSig, dataRx, yRec, csr] = ...
commlteSISO_step(nS, snrdB, prmLTEDLSCH, prmLTEPDSCH, prmMdl);
% Calculate bit errors
Measures = step(hPBer, dataIn, dataOut);
% Increase Counter
counter = counter + 1;
% Visualize constellations and spectrum
if visualsOn, zVisualize( prmLTEPDSCH, txSig, rxSig, yRec, dataRx, csr, nS);end
transmitted(counter,:) = txSig;
received(counter,:) = rxSig;
% Update subframe number
nS = nS + 2; if nS > 19, nS = mod(nS, 20); end
if counter >=5
    break;
end
end
