% PDSCH
numTx = 1; % Number of transmit antennas
numRx = 1; % Number of receive antennas
chanBW = 6; % Index to chanel bandwidth used [1,....6]
contReg = 1; % No. of OFDM symbols dedictaed to control information [1,...,3]
modType = 3; % Modulation type [1, 2, 3] for ['QPSK,'16QAM','64QAM']
% DLSCH
cRate = 1/3; % Rate matching target coding rate
maxIter = 6; % Maximum number of turbo decoding iterations
fullDecode = 0; % Whether "full" or "early stopping" turbo decoding is performed
% Channel model
chanMdl = 'frequency-selective-low-mobility';
corrLvl = 'Low';
% Simulation parametrs
Eqmode = 1; % Type of equalizer used [1,2] for ['ZF', 'MMSE']
chEstOn = 1; % Whether channel estimation is done or ideal channel model used
maxNumErrs = 5e7; % Maximum number of errors found before simulation stops
maxNumBits = 5e7; % Maximum number of bits processed before simulation stops
visualsOn = 1; % Whether to visualize channel response and constellations