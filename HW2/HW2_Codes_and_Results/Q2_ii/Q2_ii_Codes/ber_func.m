function [bler,ber] = ber_func(SNR, maxNumErrs, maxNumBits, prmLTE)
%#codegen
%% Constants
FRM=2048-24;
Kplus=FRM+24;
Indices = lteIntrlvrIndices(Kplus);
maxIter=6;
ModulationMode = 3;
noiseVar = 10.^(-SNR/10);
%% Processing loop modeling transmitter, channel model and receiver
numErrs = 0; numOfFrames = 0; nS=0; numBitErrs = 0; numBits = 0;
while ((numErrs < maxNumErrs) && (numBits < maxNumBits))
% Transmitter
u = randi([0 1], FRM,1); % Randomly generated input bits
data= CbCRCGenerator(u); % Transport block CRC code
[t1, Kplus, ~] = TbChannelCoding(data, prmLTE);
t2 = Scrambler(t1, nS); % Scrambler
t3 = Modulator(t2, ModulationMode); % Modulator
% Channel & Add AWG noise
[rxFade, ~] = MIMOFadingChan(t3, prmLTE);
c0 = AWGNChannel2(rxFade, noiseVar); % AWGN channel
% Receiver
r0 = DemodulatorSoft(c0, ModulationMode, noiseVar); % Demodulator
r1 = DescramblerSoft(r0, nS); % Descrambler
r2 = RateDematcher(r1, Kplus);
r3 = TurboDecoder_crc(-r2, Indices, maxIter); % Turbo Decoder
y = CbCRCDetector(r3); % Code block CRC detector
% Measurements
numErrs = numErrs + multiframeErr(u, y, numFrames); % Update number of bit errors
numOfFrames = numOfFrames + numFrames; % Update number of bits processed
numBitErrs = numBitErrs + sum(y~=u); % Update number of bit errors
numBits = numBits + numFrames*FRM; % Update number of bits processed
% Manage slot number with each subframe processed
nS = nS + 2; nS = mod(nS, 20);
end
%% Clean up & collect results
bler = numErrs/numOfFrames; % Compute Bit Error Rate (BER)
ber = numBitErrs/numBits; % Compute Bit Error Rate (BER)
