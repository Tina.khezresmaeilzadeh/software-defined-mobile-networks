%% Q1_b
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;
% Symbol Creation
x = zeros(block_size, 1);
x(2) = 1;
% Data Stream Creation
data = [];
for i=1:num_cols
    data = [data;x];
end

data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    ifft_data = ifft_data_matrix;
end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;



%% Q1_c

% Pass the ofdm signal through the channel
recvd_signal = ofdm_signal;

% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);
% Perform FFT
for i=1:cols_ifft_data
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));




digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));
%% Q1_d
data = randi([0 1],10240,1);
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;


data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    ifft_data = ifft_data_matrix;
end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;


% Pass the ofdm signal through the channel
recvd_signal = ofdm_signal;

% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);
% Perform FFT
for i=1:cols_ifft_data
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));




digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));
%% Q2_a
h2 = zeros(10240, 1);
delay = 5*10^(-6);
sample_time = 65.1*10^(-9);
h2(floor(delay/sample_time) + 1) = 0.8;
   

h1 = zeros(10240, 1);
h1(1) = 1;

data = randi([0 1],10240,1);
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;


data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    ifft_data = ifft_data_matrix;
    %conv_matrix(:,i) = conv(ifft_data_matrix(:,i), h1, 'same');
end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;


% Pass the ofdm signal through the channel
recvd_signal1 = conv(ofdm_signal, h1, 'same');
recvd_signal2 = conv(ofdm_signal, h2, 'same');
recvd_signal = recvd_signal1 + recvd_signal2;


% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);
% Perform FFT
for i=1:cols_ifft_data
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));




digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));
%% Q2_b
clear
clc
h2 = zeros(1024, 1);
delay = 5*10^(-6);
sample_time = 65.1*10^(-9);
h2(floor(delay/sample_time) + 1) = 0.8;
   

h1 = zeros(1024, 1);
h1(1) = 1;

data = randi([0 1],10240,1);
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;


data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

%  Cyclic Prefix Information
guard_time = 5.7*10^(-6);
sample_time = 65.1*10^(-9);


guard_len = floor(guard_time/sample_time);
guard_start = block_size-guard_len;
guard_end = block_size;


for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    %   Compute and append Cyclic Prefix
    for j=1:guard_len
       actual_guard(j,i) = ifft_data_matrix(j+guard_start,i);
    end
    %   Append the CP to the existing block to create the actual OFDM block
    ifft_data(:,i) = vertcat(actual_guard(:,i),ifft_data_matrix(:,i));

end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;


% Pass the ofdm signal through the channel
recvd_signal1 = conv(ofdm_signal, h1, 'same');
recvd_signal2 = conv(ofdm_signal, h2, 'same');
recvd_signal = recvd_signal1 + recvd_signal2;


% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);

% Remove CP
recvd_signal_matrix(1:guard_len,:)=[];


% Perform FFT
for i=1:cols_ifft_data
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));




digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));


%% Q2_c
clear
clc
h2 = zeros(1024, 1);
delay = 5*10^(-6);
sample_time = 65.1*10^(-9);
h2(floor(delay/sample_time) + 1) = 0.8;
   

h1 = zeros(1024, 1);
h1(1) = 1;

data = randi([0 1],10240,1);
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;


data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

%  Cyclic Prefix Information
cp_len = ceil(block_size);
cp_start = block_size-cp_len;
cp_end = block_size;


for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    %   Compute and append Cyclic Prefix
    for j=1:cp_len
       actual_cp(j,i) = ifft_data_matrix(j+cp_start,i);
    end
    %   Append the CP to the existing block to create the actual OFDM block
    ifft_data(:,i) = vertcat(actual_cp(:,i),ifft_data_matrix(:,i));

end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;


% Pass the ofdm signal through the channel
recvd_signal1 = conv(ofdm_signal, h1, 'same');
recvd_signal2 = conv(ofdm_signal, h2, 'same');
recvd_signal = recvd_signal1 + recvd_signal2;


% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);

% Remove CP
recvd_signal_matrix(1:cp_len,:)=[];


% Perform FFT
for i=1:cols_ifft_data
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));




digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));


%% Q3_a
clear
clc
h2 = zeros(10240, 1);
delay = 5*10^(-6);
sample_time = 65.1*10^(-9);
h2(floor(delay/sample_time) + 1) = 0.8;
   

h1 = zeros(10240, 1);
h1(1) = 1;

data = randi([0 1],10240,1);
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;


data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    ifft_data = ifft_data_matrix;
end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;


% Pass the ofdm signal through the channel
recvd_signal1 = ifft(fft(ofdm_signal).*fft(h1'));
recvd_signal2 = ifft(fft(ofdm_signal).*fft(h2'));
recvd_signal = recvd_signal1 + recvd_signal2;


% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);
% Perform FFT
for i=1:cols_ifft_data
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));


digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));


%% Q3_c
clear
clc
h2 = zeros(10240, 1);
delay = 5*10^(-6);
sample_time = 65.1*10^(-9);
h2(floor(delay/sample_time) + 1) = 0.8;
   

h1 = zeros(10240, 1);
h1(1) = 1;
data = randi([0 1],10240,1);
% Initialization
block_size = 1024;
data_length = 10240;
num_cols = data_length/block_size;


data_matrix = reshape(data, block_size, num_cols);
no_of_ifft_points = block_size;         
no_of_fft_points = block_size;

for i=1:num_cols
    ifft_data_matrix(:,i) = ifft((data_matrix(:,i)),no_of_ifft_points);
    ifft_data = ifft_data_matrix;
end

[rows_ifft_data cols_ifft_data]=size(ifft_data);
len_ofdm_data = rows_ifft_data*cols_ifft_data;

%   Actual OFDM signal to be transmitted
ofdm_signal = reshape(ifft_data, 1, len_ofdm_data);
figure(1)
plot(real(ofdm_signal)); xlabel('Time'); ylabel('Amplitude');
title('OFDM Signal');grid on;


% Pass the ofdm signal through the channel
recvd_signal1 = ifft(fft(ofdm_signal).*fft(h1'));
recvd_signal2 = ifft(fft(ofdm_signal).*fft(h2'));
recvd_signal = recvd_signal1 + recvd_signal2;


% Convert Data back to "parallel" form to perform FFT
recvd_signal_matrix = reshape(recvd_signal,rows_ifft_data, cols_ifft_data);
H = fft(h1 + h2, no_of_fft_points);
% Perform FFT
for i=1:cols_ifft_data
    
    for j=1:rows_ifft_data
        recvd_signal_matrix(j,i) = recvd_signal_matrix(j,i)/H(j);
    end
    %   FFT
    fft_data_matrix(:,i) = fft(recvd_signal_matrix(:,i),no_of_fft_points);
end

% Convert to serial stream
recvd_serial_data = reshape(fft_data_matrix, 1,(block_size*num_cols));


digital_y = real(recvd_serial_data);
for i=1:size(digital_y, 2)
    if digital_y(i) > 0.5
        digital_y(i) = 1;
    else
        digital_y(i) = 0;
    end
end
figure(2)
plot(real(digital_y));
grid on;xlabel('data points');ylabel('received data phase representation');title('Received Data "X"')   

disp(sum(abs(digital_y - data'))/size(digital_y, 2));
